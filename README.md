# webhook app make auto-replying on facebook pages with Node + Express + mongoDb

**Prerequisites**: [Node.js] - [MongoDb Host] - [Facebook Meta App] - [Ngrok Host]

## Getting Started

To install this sample, run the following commands:

```bash
git clone https://gitlab.com/Abd-lkareem/facebook_webhook.git
cd facebook_webhook
npm i
```

This will get the project installed locally.


## Launching the app

Now you can launch the app:


```bash
npm start
```
Alternativelly, you can run one of the following apps:
`nodemon app.js`

The app runs by default on port 8000 or `you can change it from app file`

## link between ngrok and your Nodejs app

- get a host from ngrok and install the ngrok-command app
- run ngrok App 
- type this command : 
```bash
ngrok http --domain=[Your_ngrok_Domain] 8000 
```

 **Note :**

 nodejs app and the ngrok should run on the same port


## Author : 
`abdulkareem Soufan`
 
