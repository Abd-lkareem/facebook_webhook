const { Router } = require('express');
const app = Router();
const homeControllers = require('./controllers/HomeController');
const bodyParser = require('body-parser');
const express = require("express");

const fs = require('fs')


app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    }),
);

app.get('/' ,  (req , res)=>{
    res.render('register')
});

app.post('/getAccess', homeControllers.getAccessToPage);

app.post('/change_key_word', homeControllers.EditPageDetails);

app.post("/webhooks",homeControllers.webhooks);

app.get('/update' ,  (req , res)=>{
    res.render('update')
});
app.get('/getResult' ,(req , res)=>{

    res.render('result' , {status : req.query.status , message : req.query.message})

});

module.exports = app;