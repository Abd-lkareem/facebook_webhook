var express = require('express');
var app = express();
// var axios = require('axios')
const bodyParser = require('body-parser');
const cors = require('cors');
var routes = require('./routes')
var connect = require('./connectToDb');
var path = require("path")


// Global Variables - these are user_related variables , so you should change them to your own

app.locals.db = connect.connect().db('facebook_webhooks');
app.locals.userScope_id = "122106183998031487"

app.use(express.static('public'));

app .use(routes)
app.use(express.json());

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    }),
);

app.use(cors())
app.set('view engine', 'ejs');


var server = app.listen(8000, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
})