// const axios = require("axios");
const axios = require("axios");
const express = require("express");



// function for general using to make comments ..
function reply_On_Comment(comment_id , page_access_tokens , from , reply){

        axios.post('https://graph.facebook.com/v17.0/' + comment_id + '/comments',
            {message: reply, access_token: page_access_tokens})
            .then(resp => {
                console.log("replay is added from " + from);
                console.log("------------------------------------------------------------- ");
            })
        return true;

}

// function for general using to make comments with photos
function reply_On_Comment_with_photo(comment_id , page_access_tokens ,from , reply , image_url){

    axios.post('https://graph.facebook.com/v17.0/' + comment_id + '/comments',
        {
            message: reply,
            access_token: page_access_tokens ,
            attachment_type : "photo" ,
            attachment_url : image_url

        })
        .then(resp => {
            console.log("replay is added from " + from);
            console.log("------------------------------------------------------------- ");
        })
    return true;

}


exports.index = (req , res)=>{
    res.render('home');
}

// get app subscribed to this page and store page info in db
exports.getAccessToPage  = async (req, res) => {

    const pageId = req.body.page_Id;
    const key_word = req.body.key_word
    const reply_message = req.body.reply_message
    const userScope_id = req.app.locals.userScope_id;
    const db = req.app.locals.db;




    const access_tokens = db.collection('access_tokens');
    const user_token = await access_tokens.findOne({"page_name": "user_token"});


    await axios.get("https://graph.facebook.com/v17.0/" + userScope_id + "/accounts?access_token=" + user_token.access_token)
        .then(async resp => {

            await resp.data.data.forEach(async (element) => {


                if (element.id === pageId) {

                    let page = await access_tokens.findOne({"page_id" : pageId});

                    if (page === null) {

                        access_tokens.insertOne({
                            page_id: element.id,
                            page_name: element.name,
                            access_token: element.access_token,
                            key_word: key_word,
                            reply_message: reply_message,
                            created_at: new Date()
                        })

                        axios.post('https://graph.facebook.com/' + element.id + '/subscribed_apps',
                            {
                                subscribed_fields: ["feed" , "messages"],
                                access_token: element.access_token
                            })
                            .then(resp => {
                                if (resp.data.success)
                                    res.redirect("/getResult?status=success&message=the setup made successfully");
                            })
                            .catch(error => {
                                res.redirect("/getResult?status=fails&message=the setup fails");
                            })


                    } else {
                        res.redirect("/getResult?status=existed&message=the page is already registred");
                    }
                }});



        })
        .catch(async error =>{
            console.error(error)
        })


}

// edit page info in db
exports.EditPageDetails  = async (req, res) => {



        const pageId = req.body.page_Id;
        const key_word = req.body.key_word
        const reply_message = req.body.update_reply_message
        const userScope_id = req.app.locals.userScope_id;
        const db = req.app.locals.db;
        const access_tokens = db.collection('access_tokens');

        let page = await access_tokens.updateOne({ page_id: pageId } ,{$set : { key_word: key_word   ,reply_message: reply_message} })

        if(page.acknowledged){
            res.redirect("/getResult?status=success&message=updating made successfully ");
        }
        else{
            res.redirect("/getResult?status=fails&message=updating fails");
        }











}

// the main webhook event , listen to publishing post and comments on the page
exports.webhooks = async (req, res) => {


// handle messages events

if(typeof req.body['entry'][0]['changes'] === "undefined")
{
    res.status(200).send("EVENT_RECEIVED");
}

else
{

//     db information
    const db = req.app.locals.db;
    const posts_tb = db.collection('posts');
    const access_tokens_tb = db.collection('access_tokens');



//     Handle Feed Events

try {
    let eventType = (JSON.stringify(req.body['entry'][0]['changes'][0]['field'])).replace(/['"]+/g, '');
    let verbType = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['verb']).replace(/['"]+/g, '');
    let itemType = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['item']).replace(/['"]+/g, '');
    let post_id = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['post_id']).replace(/['"]+/g, '');
    let page_id = JSON.stringify(req.body['entry'][0]['id']).replace(/['"]+/g, '');
    let from = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['from']['name']).replace(/['"]+/g, '');
    let from_id = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['from']['id']).replace(/['"]+/g, '');


//     handle adding post event

    if (eventType === "feed" && itemType === "status" && verbType === "add") {

        let post_text = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['message']).replace(/['"]+/g, '');
        let post = await posts_tb.findOne({post_id: post_id});
        let page = await access_tokens_tb.findOne({page_id: page_id});

        if (post === null) {

            if (post_text.includes(page.key_word)) {
                let add = await posts_tb.insertOne({
                    post_id: post_id,
                    page_id: page_id,
                    key_word: page.key_word,
                    created_at: new Date()
                })
                if (add.acknowledged) {
                    console.log("post is added from : " ,  from , " page")
                    console.log("--------------------------------------------------------")
                }
            } else {
                console.log("not interested post ")
                console.log("--------------------------------------------------------")

            }


        } else {
            console.log("the post is existed ..")
            console.log("--------------------------------------------------------")
        }

    }

//     handle Comment event

    if (eventType === "feed" && verbType === "add" && itemType === "comment" && from_id != page_id) {

        let parent_id = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['post_id']).replace(/['"]+/g, '');
        let post_id = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['parent_id']).replace(/['"]+/g, '');
        let comment_id = JSON.stringify(req.body['entry'][0]['changes'][0]['value']['comment_id']).replace(/['"]+/g, '');
        let page_id = JSON.stringify(req.body['entry'][0]['id']).replace(/['"]+/g, '');

        let post = await posts_tb.findOne({post_id: post_id});
        let page = await access_tokens_tb.findOne({page_id: page_id})
        let page_access_token = page.access_token;
        let reply = "اضغط على زر messenger لكي يصلك السعر";
        let image_url = "https://racer-ace-fully.ngrok-free.app/A.png";

        if (post === null) {
        } else {
            // check if the comment is the first one
            if (parent_id === post_id) {
                axios.get("https://graph.facebook.com/v17.0/" + comment_id + "/comments?access_token=" + page_access_token)
                    .then(resp => {

                        // check that comment has no reply

                        if (resp.data.data.length === 0) {
                            reply_On_Comment(comment_id, page_access_token, page.page_name, "❤");
                            reply_On_Comment(comment_id, page_access_token, page.page_name, "🌷");
                            setTimeout(function () {

                                reply_On_Comment_with_photo(comment_id , page_access_token , page , reply  , image_url )
                            }, 1000);
                        } else {
                            if (resp.data.data[0]['from']['name'] === page.page_name) {

                                console.log("replay is already added ! ");
                                console.log("----------------------------------------------------------");

                            } else {
                                reply_On_Comment(comment_id, page_access_token, page.page_name, "❤");
                                reply_On_Comment(comment_id, page_access_token, page.page_name, "🌷");
                                setTimeout(function () {
                                    reply_On_Comment_with_photo(comment_id , page_access_token , page , reply  , image_url )
                                }, 1000);
                            }
                        }
                    })
                    .catch(error => {
                        console.log("an error occured ..")
                        console.log("--------------------------------------------------------")
                    })
            }

        }


    }
}
catch (e) {
    console.log("an acceiden event ..")
    console.log("--------------------------------------------------------")
}
}

}
